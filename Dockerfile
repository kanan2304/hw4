FROM openjdk:18-alpine
COPY build/libs/demo-0.0.1-SNAPSHOT.jar /carapp/
CMD ["java", "-jar", "/carapp/demo-0.0.1-SNAPSHOT.jar"]