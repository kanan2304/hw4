package com.example.demo.controller;

import com.example.demo.CarService;
import com.example.demo.dto.CarDto;
import com.example.demo.dto.CreateCarDto;
import com.example.demo.dto.UpdateCarDto;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/car")
public class CarController {
    private final CarService carService;

    public CarController(CarService carService){
        this.carService=carService;
    }
//    @GetMapping
//    public List<CarDto> getAll(){
//        return carService.getAll();
//    }
    @PostMapping
    public void create(@RequestBody CreateCarDto car){
        carService.create(car);
    }
    @PutMapping
    public void update(@RequestBody UpdateCarDto car){
        carService.update(car);
    }
    @GetMapping("/id")
    public CarDto get(@PathVariable Integer id){
        return carService.get(id);
    }
    @DeleteMapping("/year-deletion")
    public void delete(@PathVariable Integer year){
        carService.delete(year);
    }






//    @GetMapping("/identification-mph")
//    public void readMph(@RequestBody Integer mph){
//        carService.readMph(mph);
//    }
//    @PostMapping("/new-mph")
//    public void createMph(@RequestBody Car car){
//        carService.createMph(car);
//    }
//    @PutMapping("/update-mph")
//    public void updateYMph(@RequestBody Car car){
//        carService.updateMph(car);
//    }
//    @DeleteMapping("/{mph}")
//    public void deleteMph(@PathVariable Integer mph){
//        carService.deleteMph(mph);
//    }
}