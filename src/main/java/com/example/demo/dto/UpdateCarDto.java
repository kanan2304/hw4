package com.example.demo.dto;

import lombok.Data;

@Data
public class UpdateCarDto {
    Integer id;
    Integer mph;

    String color;
    String engine;
    String model;
}