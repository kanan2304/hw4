package com.example.demo.dto;

import lombok.Data;

@Data
public class CarDto {
    Integer id;
    Integer mph;

    String color;
    String engine;
    String model;
}