package com.example.demo.dto;

import lombok.Data;

@Data
public class CreateCarDto {
    Integer mph;

    String color;
    String engine;
    String model;
    String password;
}