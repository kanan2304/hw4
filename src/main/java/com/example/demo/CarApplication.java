package com.example.demo;

import com.example.demo.model.CarApp;
import com.example.demo.model.MarketApp;
import com.example.demo.model.MarketApp;
import com.example.demo.repository.CarRepository;
import com.example.demo.repository.MarketRepository;
import com.example.demo.service.TruckService;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication
@RequiredArgsConstructor
public class CarApplication implements CommandLineRunner {
//	private final TruckService truckService;
	private final CarRepository carRepository;
	private final MarketRepository marketRepository;
	public static void main(String[] args) {
		SpringApplication.run(CarApplication.class, args);
	}
	@Override
	public void run(String... args) throws Exception{
		MarketApp marketApp1 = MarketApp.builder()
				.marketName("First branch")
				.build();
		MarketApp marketApp2 = MarketApp.builder()
				.marketName("Second branch")
				.build();
		marketRepository.save(marketApp1);
		marketRepository.save(marketApp2);



//		CarApp a = new CarApp();
//		a.setId(1);
//		a.setMph(200);
//		a.setPassword("car123");
//		a.setEngine("turbo");
//		a.setModel("Tesla");
//		a.setColor("Black");
//		carRepository.save(a);


	}
}