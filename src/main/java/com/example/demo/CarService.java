package com.example.demo;

import com.example.demo.dto.CarDto;
import com.example.demo.dto.CreateCarDto;
import com.example.demo.dto.UpdateCarDto;
import com.example.demo.model.CarApp;
import com.example.demo.repository.CarRepository;
import jakarta.persistence.EntityManagerFactory;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CarService {
    private final CarRepository carRepository;
    private final ModelMapper modelMapper;
    private final EntityManagerFactory emf;
    public CarService(CarRepository carRepository, ModelMapper modelMapper, EntityManagerFactory emf) {
        this.carRepository = carRepository;
        this.modelMapper= modelMapper;
        this.emf = emf;
    }
    public CarApp save(CarApp student) {
        return carRepository.save(student);
    }

    //    public List<CarDto> getAll(){
//        List<Car> cars = carRepository.findAll();
//        return (List<CarDto>) cars.stream()
//                .map(car -> modelMapper.map(car, CarDto.class));
//    }
    public CarDto get(Integer id) {
        CarApp carApp = carRepository.findById(id).get();
//        CarDto carDto = new CarDto();
//        carDto.setColor(car.getColor());
//        carDto.setId(car.getId());
//        carDto.setMph(car.getMph());
//        carDto.setModel(car.getModel());
//        carDto.setEngine(car.getEngine());
        CarDto carDto = modelMapper.map(carApp, CarDto.class);
        return carDto;
    }
    public void create(CreateCarDto dto) {
//        Car car = new Car();
//        car.setColor(dto.getColor());
//        car.setMph(dto.getMph());
//        car.setEngine(dto.getEngine());
//        car.setModel(dto.getModel());
//        car.setPassword(dto.getPassword());
        CarApp carApp = modelMapper.map(dto, CarApp.class);
        carRepository.save(carApp);
    }
    public void update(UpdateCarDto dto) {
        Optional<CarApp> entity = carRepository.findById(dto.getId());
        entity.ifPresent(car1 -> {
            car1.setColour(dto.getColor());
            car1.setModel(dto.getModel());
            car1.setMph(dto.getMph());
            car1.setEngine(dto.getEngine());
        });
    }
    public void delete(Integer year) {
        carRepository.deleteById(year);
    }





//    public void readMph(Integer mph) {
//    }
//    public void createMph(Car car) {
//    }
//    public void updateMph(Car car) {
//    }
//    public void deleteMph(Integer mph) {
//    }
}