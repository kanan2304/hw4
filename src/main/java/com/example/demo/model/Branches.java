package com.example.demo.model;

import jakarta.persistence.*;
import liquibase.change.DatabaseChange;
import lombok.*;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Branches {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    String branchName;

    @OneToOne
    @ToString.Exclude
    private Address address;
    @OneToOne
    @ToString.Exclude
    private Branches branches;
}
