package com.example.demo.model;

import jakarta.persistence.*;
import lombok.*;

@Data
@Entity
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class MarketApp {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    String marketName;

    @OneToMany
    @ToString.Exclude
    private Branches branches;
    @OneToMany
    @ToString.Exclude
    private Address address;
}