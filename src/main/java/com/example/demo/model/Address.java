package com.example.demo.model;

import jakarta.persistence.*;
import lombok.*;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Address {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    String addressName;

    @OneToOne
    @ToString.Exclude
    private Branches branches;
    @OneToOne
    @ToString.Exclude
    private MarketApp marketApp;
}
