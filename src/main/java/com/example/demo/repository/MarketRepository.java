package com.example.demo.repository;

import com.example.demo.model.MarketApp;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MarketRepository extends JpaRepository<MarketApp, Long> {
}
