package com.example.demo.repository;

import com.example.demo.model.CarApp;
import org.springframework.data.jpa.repository.JpaRepository;
public interface CarRepository extends JpaRepository<CarApp, Integer> {
}